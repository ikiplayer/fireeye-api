<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/api/customer');
        $response->assertStatus(200);
    }

    public function testRequiredFields(){

        $response = $this->post('/api/customer');
        $response->assertStatus(500);
        $response->assertJson([
                    'message' => 'Error saving record.',
                    'data' => 
                        [
                        'name' => ['The name field is required.'],
                        'mobile_number' => ['The mobile number field is required.'],
                        'email' => ['The email field is required.']
                        ]
                ]
            );

    }

    public function testStore(){
        $customer = \App\Models\Customer::factory(1)->create();
        $payload = [
            'name' => $customer[0]->name,
            'mobile_number' => $customer[0]->mobile_number,
            'email' => 'ikiplayer@hotmail.com'
        ];
        $validResponse = $this->post('/api/customer', $payload);
        $validResponse->assertStatus(200);
        $validResponse->assertJson([
            'message' => 'Record successfully created',
            'data' => [
                'email' => 'ikiplayer@hotmail.com',
                'name' =>  $customer[0]->name,
                'mobile_number' => $customer[0]->mobile_number
            ]

        ]);


    }

    public function testUpdate(){
        $customer = \App\Models\Customer::factory(1)->create();
        $payload = [
            'id' => $customer[0]->id,
            'name' => $customer[0]->name,
            'mobile_number' => $customer[0]->mobile_number,
            'email' => 'ikiplayer@hotmail.com'
        ];
        $validResponse = $this->put('/api/customer/'.$customer[0]->id, $payload);
        $validResponse->assertStatus(200);
        $validResponse->assertJson([
            'message' => 'Record successfully updated',
            'data' => [
                'id' => $customer[0]->id,
                'email' => 'ikiplayer@hotmail.com',
                'name' =>  $customer[0]->name,
                'mobile_number' => $customer[0]->mobile_number
            ]

        ]);

    }

    public function testIndex(){
            // factory(User::class, 30)->create();

        $customer = \App\Models\Customer::factory(2)->create();
        $validResponse = $this->get('/api/customer');
        $validResponse->assertStatus(200);
        $resultPayload = $validResponse->json();
        $validResponse->assertJson($resultPayload);
        
    }


    public function testShow(){
        $customer = \App\Models\Customer::factory(1)->create();
        $validResponse = $this->get('/api/customer/'.$customer[0]->id);
        $validResponse->assertStatus(200);
        $resultPayload = $validResponse->json();
        $validResponse->assertJson($resultPayload);
    }

    public function testDelete(){
        $customer = \App\Models\Customer::factory(1)->create();
        $validResponse = $this->delete('/api/customer/'.$customer[0]->id);
        $validResponse->assertStatus(200);
        $resultPayload = $validResponse->json();
        $validResponse->assertJson($resultPayload);
        $invalidResponse = $this->get('/api/customer/'.$customer[0]->id);
        $invalidResponse->assertStatus(404);


        

    }

    
}
