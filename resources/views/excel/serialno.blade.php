<table>
    <thead>
    <tr>
        <th>Product Code</th>
        <th>Serial No</th>
        <th>Activation Date</th>
        <th>Warrranty Expiry Date</th>
        <th>Product Expiry Date</th>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Status</th>
        <th>QR Code Link</th>
    </tr>
    </thead>
    <tbody>
    @foreach($serialnos as $serialNo)
        <tr>
            <td>{{ $serialNo->product_code }}</td>
            <td>{{ $serialNo->serial_no }}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->activation_date))}}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->warranty_expiry_date))}}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->product_expiry_date)) }}</td>
            <td>{{ optional($serialNo->customer)->name }}</td>
            <td>{{ optional($serialNo->customer)->email }}</td>
            @switch($serialNo->status)
                @case(1)
                    <td>New</td> 
                    @break
                @case(2)
                    <td>Warranty</td> 
                    @break
                @case(3)
                    <td>Replaced</td> 
                    @break
                @case(4)
                    <td>Replacement</td> 
                    @break
                @case(6)
                    <td>Product Expired</td> 
                @case(7)
                    <td>Warranty Expired</td> 
                    @break
                @default
                    <td></td>
            @endswitch
            <td><img src="{{ public_path('storage/'.$serialNo->filename)}}"/></td>
            <td>{{ env('APP_URL').'/api/qr-code?text='.$serialNo->serial_no  }}<a href="{{ env('APP_URL').'/api/qr-code?text='.$serialNo->serial_no }}"></a></td>
        </tr>
    @endforeach
    </tbody>
</table>