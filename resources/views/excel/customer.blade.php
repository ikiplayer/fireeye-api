<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Mobile No</th>
        <th>Email</th>
        <th>No of Products</th>
        <th>Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{{ $customer->name }}</td>
            <td>{{ $customer->mobile_number }}</td>
            <td>{{ $customer->email }}</td>
            <td>{{ count($customer->serialNumbers) }}</td>
            <td>{{ date("Y-m-d h:m", strtotime($customer->created_at))}}</td>
        </tr>
        <tr>
            <td></td>
            <td>Product Code</td>
            <td>Serial No</td>
            <td>Activation Date</td>
            <td>Warrranty Expiry Date</td>
            <td>Product Expiry Date</td>
            <td>Status</td>
        </tr>
        @foreach ($customer->serialNumbers as $serialNo)
        <tr>
            <td></td>
            <td>{{ $serialNo->product_code }}</td>
            <td>{{ $serialNo->serial_no }}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->activation_date))}}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->warranty_expiry_date))}}</td>
            <td>{{ date("Y-m-d h:m", strtotime($serialNo->product_expiry_date)) }}</td>
            @switch($serialNo->status)
                @case(1)
                    <td>New</td> 
                    @break
                @case(2)
                    <td>Warranty</td> 
                    @break
                @case(3)
                    <td>Replaced</td> 
                    @break
                @case(4)
                    <td>Replacement</td> 
                    @break
                @default
            @endswitch
        </tr>
        @endforeach
        <tr></tr>
    @endforeach
    </tbody>
</table>