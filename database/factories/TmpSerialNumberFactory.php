<?php

namespace Database\Factories;

use App\Models\TmpSerialNumber;
use Illuminate\Database\Eloquent\Factories\Factory;

class TmpSerialNumberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TmpSerialNumber::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
