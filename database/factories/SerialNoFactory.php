<?php

namespace Database\Factories;

use App\Models\SerialNo;
use Illuminate\Database\Eloquent\Factories\Factory;

class SerialNoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SerialNo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomIndex = SerialNo::setRandomIndex(2);

        return [
            //
            
            'product_code' => 'XYZ',
            'manufacturing_year' => '21',
            'index_number' => $randomIndex,
            'serial_no' => 'XYZ21'.$randomIndex,
            'activation_date' => $this->faker->dateTimeThisMonth(),
            'warranty_expiry_date' => $this->faker->dateTimeThisMonth(),
            'product_expiry_date' =>$this->faker->dateTimeThisMonth(),
            'customer_id' => rand(0, 100),

        ];
    }
}
