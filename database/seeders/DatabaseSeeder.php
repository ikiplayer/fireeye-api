<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Customer::factory(100)->create();
        \App\Models\SerialNo::factory(100)->create();
        \App\Models\User::factory(3)->create()
        ->each(function($user) {
            $user->role()->save(\App\Models\Role::factory()->make());
        });
    }
}
