<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSerialNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serial_numbers', function (Blueprint $table) {
            $table->id();
            $table->string('product_code');
            $table->integer('manufacturing_year');
            $table->integer('index_number');
            $table->string('serial_no');
            $table->timestamp('activation_date')->nullable();
            $table->timestamp('warranty_expiry_date')->nullable();
            $table->timestamp('product_expiry_date')->nullable();
            $table->bigInteger('customer_id')->default(0)->unsigned()->index('customer_id');
            $table->bigInteger('replacement_serial_no_id')->default(0)->unsigned()->index('replacement_serial_no_id');
            $table->timestamp('replacement_date')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('filename')->nullable();
            $table->timestamps();
            $table->softDeletes();


        });
        
    }

    /**
     * Reverse the migration.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serial_numbers');
    }
}
