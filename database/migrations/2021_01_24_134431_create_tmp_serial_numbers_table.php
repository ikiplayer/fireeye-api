<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpSerialNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_serial_numbers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id')->default(0)->unsigned()->index('customer_id');
            $table->bigInteger('serial_no_id')->default(0)->unsigned()->index('serial_no_id');
            $table->timestamps();
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_serial_numbers');
    }
}
