<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;


    public function serialNumbers()
    {
        return $this->hasMany('App\Models\SerialNo', 'customer_id', 'id');
    }

    // public function roles()
    // {
    //     return $this->belongsToMany('App\Role', 'role_user_table', 'user_id', 'role_id');
    // }

    public static function setRandomIndex($digits){
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    public function tmpSerialNumbers(){
        return $this->hasMany('App\Models\TmpSerialNumber', 'customer_id', 'id');
    }

    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];
}
