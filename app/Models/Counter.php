<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Counter extends Model
{
    use HasFactory;

    const TYPE_SERIAL_NO = 1;

    public static function generateRunningNo($type, $prefix = '', $length = '8')
    {

        $counter = self
                    ::where('type', $type)
                    ->when($prefix, function ($query) use ($prefix) {
                        return $query->where('prefix', '=', $prefix);
                    })
                    ->orderBy('number', 'desc')
                    ->lockForUpdate()
                    ->first();

        if (!$counter) {
            $counter = new self;

            $counter->prefix = $prefix;
            $counter->type = $type;
            $counter->char_length = $length;
        }

        $counter->number = $counter->number + 1;
        $counter->save();

        $format = '%s%0'.$counter->char_length.'d';
        $runningNo = sprintf($format, $counter->prefix, $counter->number);

        return $runningNo;
    }
}
