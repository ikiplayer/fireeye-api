<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SerialNo extends Model
{
    use HasFactory;
    use SoftDeletes;

    const STATUS_NEW = 1;
    const STATUS_WARRANTIED = 2;
    const STATUS_REPLACED = 3;
    const STATUS_REPLACEMENT = 4;
    const STATUS_ACTIVATED = 5;
    const STATUS_PRODUCT_EXPIRED = 6;
    const STATUS_WARRANTY_EXPIRED = 7;



    protected $table = 'serial_numbers';

    public function replacementSerialNumber()
    {
        return $this->belongsTo('App\Models\SerialNo', 'replacement_serial_no_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
    }

    public static function setRandomIndex($digits){
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }
}
