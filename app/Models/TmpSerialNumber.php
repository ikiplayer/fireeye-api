<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TmpSerialNumber extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function serialNumber()
    {
        return $this->belongsTo('App\Models\SerialNo', 'serial_no_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');

    }
}
