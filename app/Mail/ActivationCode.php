<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class ActivationCode extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(Config('mail.from.address'), 'FireEye System')
            ->subject('FireEye Activation Code: '.$this->customer->activation_code)
            ->markdown('mails.activation');
            // ->with([
            //     'customer' => $this->customer
            // ]);
       
    }
}
