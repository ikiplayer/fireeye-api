<?php


namespace App\Exports;

use App\Models\SerialNo;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SerialNosExport implements FromView
{
    protected $serialNos;

    function __construct($serialNos) {
            $this->serialNos = $serialNos;
    }


    public function view(): View
    {
        return view('excel.serialno', [
            'serialnos' => $this->serialNos
        ]);
    }
}