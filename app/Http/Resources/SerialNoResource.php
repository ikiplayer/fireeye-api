<?php

namespace App\Http\Resources;

use App\Models\Customer;
use App\Models\SerialNo;
use Illuminate\Http\Resources\Json\JsonResource;

class SerialNoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (strpos($request->path(), 'lookup') !== false) {
            return $this->lookup($request);
        }


        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'product_code' => $this->product_code,
            'manufacturing_year' =>$this->manufacturing_year,
            'index_number' => $this->index_number,
            'serial_no' => $this->serial_no,
            'activation_date' => $this->activation_date,
            'warranty_expiry_date' => $this->warranty_expiry_date,
            'product_expiry_date' => $this->product_expiry_date,
            'customer' => new CustomerResource($this->whenLoaded('customer')),
            'status' => $this->status,
            'replacement_serial_number' => new SerialNoResource($this->whenLoaded('replacementSerialNumber')),
            'qr_code_url' => env('APP_URL').'/api/qr-code?text='.$this->serial_no,
            'created_at' =>  $this->created_at

        ];
    }

    public function lookup($request)
    {
        return [
            'id' => $this->id,
            'serial_no' => $this->serial_no,
            'status' => $this->status,

        ];
    }
}
