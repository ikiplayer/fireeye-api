<?php

namespace App\Http\Controllers;

use App\Mail\ActivationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CustomerRegisterController extends \App\Http\Controllers\Controller
{
  
    public function store(Request $request)
    {

        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $customer = null;

        if (!$fails){
            $customer = \App\Models\Customer::where('email', $request->input('email'))->first();

            if (!$customer){
                $customer = new \App\Models\Customer;
                $customer->email = $request->input('email');
                $customer->name = $request->input('name');
                $customer->dial_code = $request->input('dial_code');
                $customer->mobile_number = $request->input('mobile_number');

            }
            $customer->activation_code = \App\Models\Customer::setRandomIndex(6);
            $customer->save();

             $tmpSerialNumbers = \App\Models\TmpSerialNumber::where('customer_id', $customer->id)
                    ->get();
                    
            if(count($tmpSerialNumbers) > 0){
                $tmpSerialNumbers->each->delete();
            }

            $serialNos = $request->input('serial_nos');
            if ($serialNos && count($serialNos) > 0){
                foreach ($serialNos as $key => $item) {
                    $serialNo = \App\Models\SerialNo
                        ::where('status', \App\Models\SerialNo::STATUS_NEW)
                        ->where('serial_no', $item['serial_no'])
                        ->first();

                    if ($serialNo){
                        $tmpSerialNo = new \App\Models\TmpSerialNumber();
                        $tmpSerialNo->customer_id = $customer->id;
                        $tmpSerialNo->serial_no_id = $serialNo->id;
                        $tmpSerialNo->save();
                    }
                }

            }
          
            if($request->input('send_activation_type') == 0){
                $this->sendEmail($customer);
            } else {
                // $this->sendSms($customer);
            }
        } 

        if (!$fails && $customer->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                // 'data' =>  new \App\Http\Resources\CustomerResource($customer)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
               ], 500);
        }
    }

      public function confirmActivation(Request $request)
    {

        $customer = \App\Models\Customer::where('email', $request->input('email'))->first();

        if ($customer->activation_code == $request->input('verification_code')){
            $customer->activation_status = 1;
            $customer->activation_code = null;
            $customer->save();

            $tmpSerialNumbers = \App\Models\TmpSerialNumber::where('customer_id', $customer->id)
                ->get();

            foreach ($tmpSerialNumbers as $key => $tmpItem) {
                $serialNo = \App\Models\SerialNo::find($tmpItem->serial_no_id);
                if($serialNo){
                    $serialNo->status = \App\Models\SerialNo::STATUS_WARRANTIED;
                    $serialNo->activation_date = \Carbon\Carbon::now()->toDateTimeString();
                    $serialNo->warranty_expiry_date = \Carbon\Carbon::now()->addYears(2)->toDateTimeString();
                    $serialNo->product_expiry_date = \Carbon\Carbon::now()->addYears(3)->toDateTimeString();
                    $serialNo->customer_id = $customer->id;
                    $serialNo->save();
                }
            }

            if($tmpSerialNumbers){
                $tmpSerialNumbers->each->delete();
            }

            return response()->json([
                'message' => __('Warranty(s) activated')
            ], 200);
        } else {
              return response()->json([
                'message' => __('Error invalid activation code.')
              ], 500);
        }
    }


    private function setModel(Request $request, \App\Models\Customer $customer){
        $customer->email = $request->input('email');
        $customer->name = $request->input('name');
        $customer->dial_code = $request->input('dial_code');
        $customer->mobile_number = $request->input('mobile_number');
        $customer->activation_code = \App\Models\Customer::setRandomIndex(6);
        $customer->save();

        if ($request->method() == 'post'){
              foreach ($customer->serial_numbers as $key => $value) {
                $serialNo = \App\Models\SerialNo
                    ::where('status', \App\Models\SerialNo::STATUS_NEW)
                    ->where('customer_id', 0)
                    ->where('serial_no', $value)
                    ->first();

                if ($serialNo){
                    $serialNo->customer_id = $customer->id;
                    $serialNo->activation_date = \Carbon\Carbon::now()->toDateTimeString();
                    $serialNo->warranty_expiry_date = \Carbon\Carbon::now()->addYears(2)->toDateTimeString();
                    $serialNo->product_expiry_date = \Carbon\Carbon::now()->addYears(3)->toDateTimeString();
                    $serialNo->status =  \App\Models\SerialNo::STATUS_WARRANTIED;
                }
            }
        }

        return $customer;

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'name' => 'required',
            'mobile_number' => 'required',
            'email' => 'required|email',
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

    private function sendEmail(\App\Models\Customer $customer){
        // $to_name = $customer->name;
        // $to_email = $customer->email;
        Mail::to([$customer->email])->send(new ActivationCode($customer));
    }



    private function sendSms(\App\Models\Customer $customer){
        // TODO: TEST
        $username = env('ISMS_ACCOUNT');
        $password = env('ISMS_PASSWORD');
        $message = urlencode('Activation Code: '.$customer->activation_code);
        $data = array (
        'sendid' => 'FireEye',
        'recipient' => array
        (
        array('dstno' => $customer->mobile_number, 'msg' => $message, 'type' => '1')
        ),
        'agreedterm' =>  'YES',
        // 'method' => 'isms_send_all_id'
        );
        $payload = json_encode($data);
        $ch = curl_init('https://www.isms.com.my/RESTAPI.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $headers = array(
        'Content-Type: application/json',
        'Authorization: Basic '. base64_encode("$username:$password"),
        'Content-Length: ' . strlen($payload)
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        echo $result;
        curl_close($ch);
        
    }


}
