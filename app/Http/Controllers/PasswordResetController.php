<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class PasswordResetController extends Controller
{
        /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = \App\Models\User::where('email', $request->email)->first();
        if (!$user){
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.'
            ], 404);
        }
        
        $passwordReset = \App\Models\PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::uuid()
             ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new \App\Notifications\PasswordResetRequest($passwordReset->token, $user)
            );
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }
    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        $passwordReset = \App\Models\PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (\Carbon\Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset);
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'confirm_password' => 'required|same:password',
            'token' => 'required|string'
        ]);
        $passwordReset = \App\Models\PasswordReset::where([
            ['token', $request->input('token')],
            ['email', $request->input('email')]
        ])->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        $user = \App\Models\User::where('email', $passwordReset->email)->first();
        if (!$user){
            return response()->json([
                'message' => 'We cant find a user with that e-mail address.'
            ], 404);
        }
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        return response()->json([
            'data' => [
                'user' => $user
            ]
        ]);
    }
}
