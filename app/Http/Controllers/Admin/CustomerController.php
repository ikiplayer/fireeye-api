<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CustomersExport;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Type\Integer;

class CustomerController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';

        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');

        $customers = \App\Models\Customer::with(['serialNumbers', 'serialNumbers.replacementSerialNumber'])
            //::whereBetween('reservation_from', [$from, $to])->get();
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('mobile_number', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%");

            })
         
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

        return \App\Http\Resources\CustomerResource::collection($customers);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $customer = new \App\Models\Customer;

        if (!$fails){
            $customer = $this->setModel($request, $customer);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($customer->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\CustomerResource($customer)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
               ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $customer = \App\Models\Customer
                ::with([])
                ->find($id);

        if ($customer) {
            return new \App\Http\Resources\CustomerResource($customer);
        } else {
             return response()->json([
                'message' => __('Error saving record.')
               ], 404);
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $customer = \App\Models\Customer
                        ::find($id);

        if (!$fails){
            $customer = $this->setModel($request, $customer);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($customer->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\CustomerResource($customer)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.')
               ], 500);
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
         $customer = \App\Models\Customer
                        ::find($id);

        if($customer) {
            $customer->delete();
             return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
               return response()->json([
                'message' => __('Record not found')
            ], 404);

        }

    }

    public function export()
    {
        $customers = Customer::get();
        Excel::store(new CustomersExport($customers), 'public/customers.xlsx');
        $url = Config('filesystems.disks.public.url').'/customers.xlsx';
        return response()->json(['data' => $url], 200);

    }



    private function setModel(Request $request, \App\Models\Customer $customer){


        $customer->email = $request->input('email');
        $customer->name = $request->input('name');
        $customer->dial_code = $request->input('dial_code');
        $customer->mobile_number = $request->input('mobile_number');
        $customer->save();
        $customer->load([]);


        return $customer;

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'name' => 'required',
            'mobile_number' => 'required|min:6',
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }


}
