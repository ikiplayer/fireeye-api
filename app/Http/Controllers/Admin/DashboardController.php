<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController extends \App\Http\Controllers\Controller
{
    //
    public function getDashboardInformation()
    {
        $totalSerialNo = \App\Models\SerialNo::get()->count();
        $totalNewSerialNo = \App\Models\SerialNo::get()
            ->where('status', \App\Models\SerialNo::STATUS_NEW)
            ->count();
        $totalWarrantySerialNo = \App\Models\SerialNo::get()
            ->where('status', \App\Models\SerialNo::STATUS_WARRANTIED)
            ->count();
        $totalReplacementSerialNo = \App\Models\SerialNo::get()
            ->where('status', \App\Models\SerialNo::STATUS_REPLACEMENT)
            ->count();
        $totalWarrantyExpiredSerialNo = \App\Models\SerialNo::get()
            ->where('status', \App\Models\SerialNo::STATUS_WARRANTY_EXPIRED)
            ->count();
        $totalProductExpiredSerialNo = \App\Models\SerialNo::get()
            ->where('status', \App\Models\SerialNo::STATUS_PRODUCT_EXPIRED)
            ->count();
        $totalActivationSerialNo = \App\Models\SerialNo::get()
            ->where('activation_date', '!=', null)
            ->count();
        $totalCustomer = \App\Models\Customer::get()->count();


        return response()->json([
            'data' => [
                'serial_no_total' => $totalSerialNo ,
                'serial_no_new_total' => $totalNewSerialNo ,
                'serial_no_replacement_total' => $totalReplacementSerialNo ,
                'serial_no_activation_total' => $totalActivationSerialNo ,
                'serial_no_warranty_total' => $totalWarrantySerialNo ,
                'serial_no_warranty_expired_total' => $totalWarrantyExpiredSerialNo ,
                'serial_no_product_expired_total' => $totalProductExpiredSerialNo ,
                'customer_total' => $totalCustomer 
            ]
        ]
        , 200);
    }

    public function getTotalCustomers()
    {

    }
}
