<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends \App\Http\Controllers\Controller
{
    public function show(Request $request, $id)
    {


        $user = \App\Models\User::with(['role'])->find($id);
        if($user) {
            return response()->json($user);
        }

        return response()->json(['message' => 'User not found!'], 404);
    }

    public function index()
    {
 
        switch (request()->input('sort_by')) {
            default:
                $sortBy = 'created_at';
                break;
        }


        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');
        $users = \App\Models\User::with(['role'])
            ->where('id', '!=', Auth::user()->id)
            ->whereHas('role', function ($query) {
                return $query->where('roles.role', '!=', 'superadmin');
            })
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('', 'LIKE', "%{$search}")
                        ->orWhere('', 'LIKE', "%{$search}");
            })
            ->orderBy($sortBy, $descending)
            ->get();

        return \App\Http\Resources\UserResource::collection($users);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->getValidator($request, 
        ['email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        'confirm_password' => 'required|same:password']);
        $fails = $validator->fails();

        $user = new \App\Models\User;

        if (!$fails){
            $user = $this->setModel($request, $user);
        }  else {
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
            ], 422);
        }
      

        if ($user->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\UserResource($user)
            ], 200);
        } else {
            return response()->json([
            'message' => __('Error saving record.'),
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $user = \App\Models\User
            ::find($id);

        if (!$fails){
            $user = $this->setModel($request, $user);
        } else {
            return response()->json([
                'message' => __('Error updating record.'),
                'data' =>  $validator->errors()
            ], 422);
        }

        if ($user->exists){
            return response()->json([
                'message' => __('Record successfully updated'),
                'data' =>  new \App\Http\Resources\UserResource($user)
            ], 200);
        } else {
            return response()->json([
                'message' => __('Error saving record.')
            ], 500);
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
         $user = \App\Models\User
                        ::find($id);

        if($user) {
            $user->delete();
             return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
               return response()->json([
                'message' => __('Record not found')
            ], 404);

        }

    }

    private function setModel(Request $request, \App\Models\User $user){

        $user->email = $request->input('email');
        $user->name = $request->input('name');
        if ($request->isMethod('POST')){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();

        $role = \App\Models\Role::
            where('user_id', $user->id)->
            first();
        if (!$role){
            $role = new \App\Models\Role;
            $role->user_id = $user->id;
        }

        // dd($request->input('role'));
        // TODO: FIX
        $role->role = $request->input('role');
        $role->save();

        $user->load(['role']);
        return $user;

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'name' => 'required',
        ] + $otherRules;

        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }
    
}
