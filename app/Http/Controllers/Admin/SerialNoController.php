<?php

namespace App\Http\Controllers\Admin;

use App\Exports\SerialNosExport;
use App\Models\SerialNo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\Count;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class SerialNoController extends \App\Http\Controllers\Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function lookup()
    {
        request()->input('sort_by') ? $sortBy = request()->input('sort_by') : $sortBy = 'created_at';

        $status = request()->input('status');
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';

        $serialNos = \App\Models\SerialNo::with(['replacementSerialNumber', 'customer'])
            ->when($status, function ($query) use ($status){
                return $query
                        ->where('status', $status);
            })
            ->orderBy($sortBy, $descending)
            ->get();

            return \App\Http\Resources\SerialNoResource::collection($serialNos);
    }
    public function index()
    {

        SerialNo::where('status', SerialNo::STATUS_WARRANTIED)
                ->where('warranty_expiry_date', '<', date('Y-m-d'))
                ->update(['status' => SerialNo::STATUS_WARRANTY_EXPIRED]);

        SerialNo::where('status', SerialNo::STATUS_WARRANTY_EXPIRED)
                ->where('product_expiry_date', '<', date('Y-m-d'))
                ->update(['status' => SerialNo::STATUS_PRODUCT_EXPIRED]);

        $emptyFileSerialNos = SerialNo::where('filename', '')->get();

        foreach ($emptyFileSerialNos as $key => $serialNo) {
            $image = QrCode::format('png')
                ->margin(2)
                ->errorCorrection('H')
                ->encoding('UTF-8')
                ->size(100)
                ->generate(Config('app.web_url').'/activate/'.$serialNo->serial_no);

            $outputFile = $serialNo->serial_no.'.png';
            Storage::disk('local')->put('public/'.$outputFile, $image);
            $serialNo->filename = $outputFile;
            $serialNo->save();

        }


        switch (request()->input('sort_by')) {
            case 'activation_date':
                $sortBy = 'activation_date';
                break;
            case 'warranty_expiry_date':
                $sortBy = 'warranty_expiry_date';
                break;
            case 'product_expiry_date':
                $sortBy = 'product_expiry_date';
                break;
            default:
                $sortBy = 'created_at';
                break;
        }

        
        $descending = request()->input('descending') == 'true' ? 'DESC' : 'ASC';
        $search = request()->input('search');

        $status = null;
        $activationFilter = null;

        if (request()->input('status') == 5){
            $activationFilter = true;
        } else {
            $status = request()->input('status');
        }

        $serialNos = \App\Models\SerialNo::with(['replacementSerialNumber', 'customer'])
            ->when($search, function ($query) use ($search){
                return $query
                        ->where('serial_no', 'LIKE', "%{$search}%");
            })
            ->when($activationFilter, function ($query){
                return $query
                        ->where('activation_date', '!=', null );
            })
            ->when($status, function ($query) use ($status){
                return $query
                        ->where('status', $status);
            })
            ->orderBy($sortBy, $descending)
            ->paginate(request()->input('rows_per_page'));

            return \App\Http\Resources\SerialNoResource::collection($serialNos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator($request);
        $fails = $validator->fails();

        $serialNo = new \App\Models\SerialNo;

        if (!$fails){
            $serialNo = $this->setModel($request, $serialNo);
        } 

        if (!$fails && $serialNo->exists){
            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\SerialNoResource($serialNo)
            ], 200);
        } else {
               return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()
               ], 500);
        }
    }

    public function generateNewSerialNumber(Request $request){

        $generationAmount = $request->input('generation_amount') ?: 1;
        for ($i=0; $i < $generationAmount ; $i++) { 
            try {
            $productCode = $request->input('product_code');
            $randomIndex = \App\Models\SerialNo::setRandomIndex(2);
            $manufacturingYear = $request->input('manufacturing_year');
            $runningNo = \App\Models\Counter::generateRunningNo(\App\Models\Counter::TYPE_SERIAL_NO, $productCode.$manufacturingYear, 8);
          
            $serialNo = new \App\Models\SerialNo;
            $serialNo->product_code = $productCode;
            $serialNo->manufacturing_year = $request->input('manufacturing_year');
            $serialNo->index_number = $randomIndex;
            $serialNo->serial_no = $runningNo.$randomIndex;
            $serialNo->status = \App\Models\SerialNo::STATUS_NEW;
            $serialNo->save();    
            } catch (\Throwable $th) {
                return response()->json([
                    'message' => __('Error creating serial numbers'),
                ], 500);
            }
        }

        return response()->json([
            'message' => __('Record successfully created'),
        ], 200);

    }

    public function attachSerialNumberWithReplacement(Request $request, $id, $replacementId){
        $serialNo = \App\Models\SerialNo::findOrFail($id);
        $replacementSerialNo = \App\Models\SerialNo::findOrFail($replacementId);
        if ($serialNo->status == \App\Models\SerialNo::STATUS_WARRANTIED && $replacementSerialNo->status == \App\Models\SerialNo::STATUS_NEW){
            $serialNo->replacement_serial_no_id = $replacementSerialNo->id;
            $serialNo->replacement_date = \Carbon\Carbon::now()->toDateTimeString();
            $serialNo->status = \App\Models\SerialNo::STATUS_REPLACED;
            $serialNo->save();
            $replacementSerialNo->status = \App\Models\SerialNo::STATUS_REPLACEMENT;
            $replacementSerialNo->customer_id = $serialNo->customer_id;
            $replacementSerialNo->activation_date = \Carbon\Carbon::now()->toDateTimeString();
            $replacementSerialNo->warranty_expiry_date = \Carbon\Carbon::now()->addYears(2)->toDateTimeString();
            $replacementSerialNo->product_expiry_date = \Carbon\Carbon::now()->addYears(3)->toDateTimeString();
            $replacementSerialNo->save();
            $serialNo->load(['replacementSerialNumber']);

            return response()->json([
                'message' => __('Record successfully created'),
                'data' =>  new \App\Http\Resources\SerialNoResource($serialNo)
            ], 200);
        } else {
            return response()->json([
                'message' => __('Error creating serial numbers'),
            ], 500);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SerialNo  $serialNo
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $serial_number = \App\Models\SerialNo
                ::with([])
                ->find($id);

        if ($serial_number) {
            return new \App\Http\Resources\SerialNoResource($serial_number);
        } else {
             return response()->json([
                'message' => __('Error saving record.')
               ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SerialNo  $serialNo
     * @return \Illuminate\Http\Response
     */
    public function edit(SerialNo $serialNo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SerialNo  $serialNo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SerialNo $serialNo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SerialNo  $serialNo
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        //
        $serialNo = \App\Models\SerialNo::find($id);

        if($serialNo) {
            $serialNo->delete();
            return response()->json([
                'message' => __('Record successfully deleted')
            ], 200);
        } else {
            return response()->json([
                'message' => __('Record not found')
            ], 404);
        }
    }

    public function export()
    {
        $status = request()->input('status');
        $serialNo = SerialNo::when($status, function ($query) use ($status) {
                    return $query->where('status', $status);
                })
                ->get();

        Excel::store(new SerialNosExport($serialNo), 'public/serial_numbers.xlsx');
        $url = Config('filesystems.disks.public.url').'/serial_numbers.xlsx';
        return response()->json(['data' => $url], 200);


    }

   private function setModel(Request $request, \App\Models\SerialNo $serialNo){

        if ($request->method() == 'post'){
            $serialNo->activation_date = $request->input('activation_date');
        }

        $serialNo->product_code = $request->input('product_code');
        $serialNo->manufacturing_year = $request->input('manufacturing_year');
        $serialNo->running_no = \App\Models\Counter::generateRunningNo(\App\Models\Counter::TYPE_SERIAL_NO, $request->input('product_code'), 8);
        $serialNo->index_number = $request->input('index_number');
        $serialNo->serial_no = $request->input('serial_no');
        $serialNo->activation_date = $request->input('activation_date');
        $serialNo->warranty_expiry_date = $request->input('warranty_expiry_date');
        $serialNo->product_expiry_date = $request->input('product_expiry_date');
        $serialNo->mobile_number = $request->input('mobile_number');
        $serialNo->email = $request->input('email');
        $serialNo->status = $request->input('status');

    }

    private function getValidator(Request $request, $otherRules = []){

        $rules = [
            'product_code' => 'required|min:10',
            'mobile_number' => 'required|min:10',
            'email' => 'required|emali'
        ];

        $rules = array_merge($rules, $otherRules);
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

    

}
