<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 500);
        }


        $user = \App\Models\User::create([
            'name' => $request->name, 
            'email' => $request->email, 
            'password' => bcrypt($request->password)
        ]);

        if ($user){
            $role = new \App\Models\Role();
            $role->user_id = $user->id;
            $role->role = $request->input('role') ?: 'admin';
            $role->save();
        }

        if ($user){
            return response()->json($user);
        } else {
            return response()->json([
                'message' => __('Error saving record.'),
            ], 500);
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email', 
            'password' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json([
                'message' => __('Error saving record.'),
                'data' =>  $validator->errors()->toArray()
            ], 500);
        }

        if( Auth::attempt(['email'=>$request->email, 'password'=>$request->password]) ) {
            $user = Auth::user();
            $userRole = $user->role()->first();
            $user->load(['role']);

            if ($userRole) {
                $this->scope = $userRole->role;
            }

            $token = $user->createToken($user->email.'-'.now(), [$userRole->role]);

            return response()->json([
                'data' => [
                    'user' => $user,
                    'token' => $token->accessToken
                ]
            ]);
        }
    }

    public function sendPasswordResetLink(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'message' => 'Password reset email sent.',
            'data' => $response
        ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['message' => 'Email could not be sent to this email address.']);
    }
}
