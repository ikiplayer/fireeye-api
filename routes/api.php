<?php

use Composer\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\Environment\Console;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

 

Route::post('auth/register', '\App\Http\Controllers\AuthController@register');
Route::post('auth/login', '\App\Http\Controllers\AuthController@login');
Route::post('auth/forgot', '\App\Http\Controllers\AuthController@forgot');
Route::post('auth/password-reset', '\App\Http\Controllers\PasswordResetController@reset');
Route::get('lookup/serial-number', '\App\Http\Controllers\Admin\SerialNoController@lookup');
Route::post('customer/send/activation-code', '\App\Http\Controllers\CustomerRegisterController@store');
Route::post('customer/confirm/activation-code', '\App\Http\Controllers\CustomerRegisterController@confirmActivation');



Route::group([
    'prefix' => 'admin',
    'middleware' => [
    'auth:api', 
    // 'role'
    ],
], function () {

    Route::get('user/{id}', '\App\Http\Controllers\Admin\UserController@show');
    Route::get('user', '\App\Http\Controllers\Admin\UserController@index');
    Route::post('user', '\App\Http\Controllers\Admin\UserController@store');
    Route::put('user/{id}', '\App\Http\Controllers\Admin\UserController@update');
    Route::delete('user/{id}', '\App\Http\Controllers\Admin\UserController@destroy');
    Route::get('customer/export', '\App\Http\Controllers\Admin\CustomerController@export');
    Route::apiResource('customer', '\App\Http\Controllers\Admin\CustomerController');
    Route::post('generate/serial-number', '\App\Http\Controllers\Admin\SerialNoController@generateNewSerialNumber');
    Route::post('replace/{id}/replacement/{replacementId}', '\App\Http\Controllers\Admin\SerialNoController@attachSerialNumberWithReplacement');
    Route::get('serial-number/export', '\App\Http\Controllers\Admin\SerialNoController@export');
    Route::apiResource('serial-number', '\App\Http\Controllers\Admin\SerialNoController');
    Route::get('dashboard', '\App\Http\Controllers\Admin\DashboardController@getDashboardInformation');
    Route::post('auth/send-password-reset', '\App\Http\Controllers\PasswordResetController@create');
});


Route::get('qr-code', function () {
    $text = request()->input('text') ?: env('APP_URL');
    $size = request()->input('size') ?: 300;

    $qrCode = QrCode
        ::format('png')
        ->margin(2)
        ->errorCorrection('H')
        ->encoding('UTF-8')
        ->size($size)
        ->generate(Config('app.web_url').'/activate/'.$text);

    return response($qrCode)->header('Content-Type', 'image/png');
});


